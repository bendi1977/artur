import psycopg2
from dotenv import load_dotenv
connection = None

load_dotenv("./config/.env")

try:
    connection = psycopg2.connect(
        host="172.17.0.1",
        database="testdb",
        user="testuser",
        password="qwerty123"
        )

    connection.autocommit = True

    with connection.cursor() as cursor:
        cursor.execute("SELECT version();")
        version = cursor.fetchone()[0]  # Получаем версию сервера
        print(f"Server version: {version}")

    # Создание новой таблицы
    with connection.cursor() as cursor:
        cursor.execute(
            """CREATE TABLE users(
            id serial PRIMARY KEY,
            first_name varchar(50) NOT NULL,
            nick_name varchar(50) NOT NULL);"""
        )

    print("[INFO] Table created successfully")

except Exception as ex:
    print("[INFO] Error while working with PostgreSQL", ex)

finally:
    if connection:
        connection.close()
        print("[INFO] PostgreSQL connection closed")